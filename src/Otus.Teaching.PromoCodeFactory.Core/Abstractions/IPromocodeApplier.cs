﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions
{
    public interface IPromocodeApplier
    {
        Task ApplyPromocodeAsync(Guid customerId, Guid promocodeId);
    }
}