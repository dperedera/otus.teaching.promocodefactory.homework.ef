﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomersRepository : EfRepository<Customer>, IPromocodeApplier
    {
        public CustomersRepository(EfContext context) : base(context) { }

        public async Task ApplyPromocodeAsync(Guid customerId, Guid promocodeId)
        {
            var customer = await Context.Set<Customer>()
                .Include(x => x.Promocodes)
                .FirstOrDefaultAsync(x => x.Id == customerId);
            var promocode = await Context.Set<PromoCode>().FirstOrDefaultAsync(x => x.Id == promocodeId);

            if (customer.Promocodes.All(x => x.Id != promocode.Id))
            {
                customer.Promocodes.Add(promocode);
            }

            await Context.SaveChangesAsync();
        }

        public override async Task<IEnumerable<Customer>> GetAllAsync()
        {
            var customers = await Context.Set<Customer>()
                .Include(x => x.Preferences)
                .ToListAsync();

            return customers;
        }

        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            var customer = await Context.Set<Customer>()
                .Include(x => x.Preferences)
                .FirstOrDefaultAsync(x => x.Id == id);

            return customer;
        }

        public override async Task<Customer> CreateAsync(Customer item)
        {
            await using var transasction = await Context.Database.BeginTransactionAsync();
            var customerEntity = await Context.Set<Customer>().AddAsync(new Customer());

            var customer = customerEntity.Entity;

            var preferences = await Context.Set<Preference>()
                .Where(x => item.Preferences.Select(p => p.Id).Contains(x.Id))
                .ToListAsync();

            customer.Email = item.Email;
            customer.FirstName = item.FirstName;
            customer.LastName = item.LastName;
          customer.DateCreated = DateTime.Today;

            await Context.SaveChangesAsync();

            preferences.ForEach(x => Context.Set<CustomerPreference>()
                .Add(new CustomerPreference
                {
                    Preference = x,
                    CustomerId = customer.Id
                }));

            await Context.SaveChangesAsync();

            await transasction.CommitAsync();
            return customer;
        }

        public override async Task UpdateAsync(Guid id, Customer item)
        {
            var customer = await Context.Set<Customer>().FirstOrDefaultAsync(x => x.Id == id);
            if (customer == null)
            {
                throw new ArgumentException($"Customer with id {id} not found");
            }

            var preferenceIds = item.Preferences.Select(x => x.Id);

            var preferences = await Context.Set<Preference>()
                .Where(x => preferenceIds.Contains(x.Id))
                .ToListAsync();

            customer.Email = item.Email;
            customer.FirstName = item.FirstName;
            customer.LastName = item.LastName;

            var existingPreferences = await Context.Set<CustomerPreference>()
                .Where(x => x.CustomerId == id)
                .ToListAsync();

            Context.Set<CustomerPreference>().RemoveRange(existingPreferences);

            preferences.ForEach(x => Context.Set<CustomerPreference>()
                .Add(new CustomerPreference
                {
                    Preference = x,
                    CustomerId = id
                }));

            await Context.SaveChangesAsync();
        }
    }
}