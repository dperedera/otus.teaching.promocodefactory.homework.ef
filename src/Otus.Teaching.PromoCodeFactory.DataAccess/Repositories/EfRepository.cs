﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly EfContext Context;

        public EfRepository(EfContext context)
        {
            Context = context;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await Context.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public virtual async Task<T> CreateAsync(T entity)
        {
            var item = Context.Add(entity);
            await Context.SaveChangesAsync();
            return entity;
        }

        public virtual async Task UpdateAsync(Guid id, T entity)
        {
            var item = await Context.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            item = entity;
            await Context.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(Guid id)
        {
            var item = await Context.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            Context.Set<T>().Remove(item);
            await Context.SaveChangesAsync();
        }
    }
}