﻿using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer : IDbInitializer
    {
        private readonly EfContext _efContext;

        public void InitializeDb()
        {
            _efContext.Database.Migrate();
        }

        public EfDbInitializer(EfContext efContext)
        {
            _efContext = efContext;
        }
    }
}