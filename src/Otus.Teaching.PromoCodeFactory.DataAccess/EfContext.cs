﻿using System.Net.Mime;
using System.Reflection.PortableExecutable;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
	public sealed class EfContext : DbContext
	{
		private const int MaxLength = 150;
		public DbSet<Employee> Employees { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<Preference> Preferences { get; set; }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<PromoCode> PromoCodes { get; set; }
		
		public EfContext()
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			BuildCustomers(modelBuilder);
			BuildRoles(modelBuilder);
			BuildEmployees(modelBuilder);
			BuildPreferences(modelBuilder);
			modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
			modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
			modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
			modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
		}

		private static void BuildCustomers(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Customer>()
				.ToTable("Customers")
				.HasKey(x => x.Id);

			modelBuilder.Entity<Customer>()
				.Property(x => x.FirstName)
				.HasMaxLength(MaxLength);

			modelBuilder.Entity<Customer>()
				.Property(x => x.LastName)
				.HasMaxLength(MaxLength);

			modelBuilder.Entity<Customer>()
				.Property(x => x.Email)
				.HasMaxLength(MaxLength);

			modelBuilder.Entity<Customer>()
				.Ignore(x => x.FullName);
		}

		private static void BuildRoles(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Role>()
				.ToTable("Roles")
				.HasKey(x => x.Id);

			modelBuilder.Entity<Role>()
				.Property(x => x.Description)
				.HasMaxLength(MaxLength);

			modelBuilder.Entity<Role>()
				.Property(x => x.Name)
				.HasMaxLength(MaxLength);
		}

		private static void BuildEmployees(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Employee>()
				.ToTable("Employees")
				.HasKey(x => x.Id);

			modelBuilder.Entity<Employee>()
				.Property(x => x.FirstName)
				.HasMaxLength(MaxLength);

			modelBuilder.Entity<Employee>()
				.Property(x => x.LastName)
				.HasMaxLength(MaxLength);

			modelBuilder.Entity<Employee>()
				.Property(x => x.Email)
				.HasMaxLength(MaxLength);

			modelBuilder.Entity<Employee>()
				.Ignore(x => x.FullName);
			
			modelBuilder.Entity<Employee>()
				.HasOne(x => x.Role)
				.WithMany()
				.HasForeignKey(x => x.RoleId);
		}
		
		private static void BuildPreferences(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Preference>()
				.ToTable("Preferences")
				.HasKey(x => x.Id);

			modelBuilder.Entity<Preference>()
				.Property(x => x.Name)
				.HasMaxLength(MaxLength);
		}
		
	

		public EfContext(DbContextOptions options) : base(options)
		{
		}
		
	}
}