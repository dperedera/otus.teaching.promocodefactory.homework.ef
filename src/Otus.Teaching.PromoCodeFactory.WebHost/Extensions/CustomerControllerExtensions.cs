﻿// using System.Linq;
// using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
// using Otus.Teaching.PromoCodeFactory.WebHost.Models;
//
// namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
// {
//     public static class CustomerControllerExtensions
//     {
//         public static CustomerShortResponse ToCustomerShortResponse(this Customer customer)
//         {
//             return  new CustomerShortResponse
//             {
//                 Email = customer.Email,
//                 Id = customer.Id,
//                 FirstName = customer.FirstName,
//                 LastName = customer.LastName
//             };
//         }
//         public static CustomerResponse ToCustomerResponse(this Customer customer)
//         {
//             return  new CustomerResponse
//             {
//                 Email = customer.Email,
//                 Id = customer.Id,
//                 FirstName = customer.FirstName,
//                 LastName = customer.LastName,
//                 Preferences = customer.Preferences,
//                 PromoCodes = customer.Promocodes.Select(x => x.ToPromoCodeShortResponse()).ToList()
//             };
//         }
//         // ReSharper disable once MemberCanBePrivate.Global    
//         public static PromoCodeShortResponse ToPromoCodeShortResponse(this PromoCode promoCode)
//         {
//             return  new PromoCodeShortResponse
//             {
//                Id = promoCode.Id,
//                Code = promoCode.Code,
//                BeginDate = promoCode.BeginDate.ToString("yyyy-MM-dd"),
//                EndDate = promoCode.EndDate.ToString("yyyy-MM-dd"),
//                PartnerName = promoCode.PartnerManager?.FullName,
//                ServiceInfo = promoCode.ServiceInfo
//             };
//         }
//     }
// }