﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    ///     Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customersRepository;

        public CustomersController(IRepository<Customer> customersRepository)
        {
            _customersRepository = customersRepository;
        }

        /// <summary>
        ///     Получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customersRepository.GetAllAsync();

            return Ok(customers.Select(x => new CustomerShortResponse
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }));
        }

        /// <summary>
        ///     Получить клиента по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(new CustomerResponse
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PromoCodes = customer.Promocodes?.Select(x => new PromoCodeShortResponse
                {
                    Id = x.Id,
                    Code = x.Code,
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString("O"),
                    EndDate = x.EndDate.ToString("O")
                }),
                Preferences = customer.Preferences?.Select(x => new PreferenceResponse
                {
                    Id = x.Id,
                    Name = x.Name
                })
            });
        }

        /// <summary>
        ///     Создать клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Preferences = request.PreferenceIds.Select(x => new Preference {Id = x}).ToList()
            };

            await _customersRepository.CreateAsync(customer);

            return Ok();
        }


        /// <summary>
        ///     Изменить клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = new Customer
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Preferences = request.PreferenceIds.Select(x => new Preference {Id = x}).ToList()
            };

            await _customersRepository.UpdateAsync(id, customer);

            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _customersRepository.DeleteAsync(id);

            return Ok();
        }
    }
}