﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    ///     Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private readonly IRepository<Preference> _preferencesRepository;

        public PreferenceController(IRepository<Preference> preferencesRepository)
        {
            _preferencesRepository = preferencesRepository;
        }

        /// <summary>
        ///     Получение списка предпочтений
        /// </summary>
        /// <returns></returns>
        [SwaggerResponse(200, typeof(IEnumerable<PreferenceResponse>))]
        public async Task<IActionResult> GetPreferences()
        {
            var preferences = await _preferencesRepository.GetAllAsync();

            return Ok(preferences.Select(x => new PreferenceResponse
            {
                Id = x.Id,
                Name = x.Name
            }));
        }
    }
}