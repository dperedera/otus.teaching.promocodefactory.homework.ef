﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    ///     Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IPromocodeApplier _promocodeApplier;
        private readonly IRepository<PromoCode> _promocodesRepository;

        public PromocodesController(IRepository<PromoCode> promocodesRepository, IRepository<Customer> customersRepository,
            IRepository<Preference> preferenceRepository, IPromocodeApplier promocodeApplier)
        {
            _promocodesRepository = promocodesRepository;
            _customersRepository = customersRepository;
            _preferenceRepository = preferenceRepository;
            _promocodeApplier = promocodeApplier;
        }

        /// <summary>
        ///     Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodesRepository.GetAllAsync();

            return Ok(promocodes.Select(x => new PromoCodeShortResponse
            {
                Id = x.Id,
                BeginDate = x.BeginDate.ToString("O"),
                EndDate = x.EndDate.ToString("O"),
                Code = x.Code,
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            }));
        }

        /// <summary>
        ///     Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var preference =
                preferences.FirstOrDefault(x => x.Name.Equals(request.Preference, StringComparison.InvariantCultureIgnoreCase));

            if (preference == null)
            {
                return NotFound();
            }

            var promocode = await _promocodesRepository.CreateAsync(new PromoCode
            {
                Code = request.PromoCode,
                Preference = preference,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(30),
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo
            });

            var customers = await _customersRepository.GetAllAsync();
            var customersToApplyPromocode = customers.Where(x => x.Preferences.Select(p => p.Id).Any(pid => pid == preference.Id)).ToList();

            customersToApplyPromocode.ForEach(async x => await _promocodeApplier.ApplyPromocodeAsync(x.Id, promocode.Id));

            return Ok();
        }
    }
}